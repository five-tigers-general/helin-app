/*
 * 接口配置、请求方法、公共方法 
 * Auth:wuml
 */

// 开发环境
// var HOST = 'http://39.104.203.128:8083/app/'
var HOST = 'http://192.168.101.60:8080/'
var IMGPATH = 'http://39.104.203.128:8083/'

export const config = {
	HOST,
	IMGPATH,
	update: `app/checkAppVersion`,
	login: `app/login`,
	logout: `app/logout`,
	getBanner:`app/getTitleNews`,
	getNewsList:`app/getNewsList`,
	getNewsById:`app/getNewsById`,
	getDictByType:`app/getDictByType`,
	getMonitor:`/mp/em/info`
}

export const $$ = {
	getVersion:function(callback){
		plus.runtime.getProperty(plus.runtime.appid, (wgtinfo) => {
			console.log("getVersion")
			return callback && callback(wgtinfo)
		})
	},
	getUpdate: function(isPrompt,callback) {
		let version
		$$.getVersion((v)=>{
			var data = {
				appid: plus.runtime.appid,
				versionCode: parseInt(v.versionCode),
				versionName: v.version,
				// imei: plus.device.imei
			}
			// var data = {
			// 	appid: "34432432243",
			// 	versionCode: 99,
			// 	versionName: "2343",
			// 	// imei: plus.device.imei
			// }
			$$.request({
				url: config.update,
				data: data,
				method:"POST"
			}, isPrompt, false).then((res) => {
				var data = res.data
				if (!$$.isEmptyObj(data) && $$.isNotNull(data.downloadUrl)) {
					$$.updateModal(data)
				} else {
					// 已是最新版
					if(isPrompt){
						uni.showToast({
							title: "暂无新版本",
							icon: "none"
						});
					}
				}
			}).catch((e) => {
				console.log(e.errMsg)
			}).finally(()=>{
				console.log("complate")
			})
		})
	},
	updateModal:function(data){
		uni.showModal({
			title: '提示',
			content: '发现新版本',
			showCancel: !data.forceUpdate,
			cancelText: '暂不更新',
			confirmText: '立即更新',
			success: res => {
				if(res.confirm) {
					var updateData = JSON.stringify(data)
					uni.reLaunch({
						url:"/pages/update/update?data="+updateData
					})
				}
			},
			fail: () => {},
			complete: () => {}
		});
	},
	request: function(options, isLoading, isLogin) {
		if (isLoading) {
			// 显示加载
			uni.showLoading({
				title: ""
			})
		}
		
		var defaultOption = {
			url: config.HOST + options.url,
			data: options.data || {},
			method: options.method || 'GET',
			header: options.method === 'POST' ? {
				'cache-control': "no-cache, no-store, max-age=0, must-revalidate",
				'Content-Type': "application/json;charset=utf-8",
			} : {
				'X-Requested-With': 'XMLHttpRequest',
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
			},
			dataType: 'json',
		}
		if (isLogin) {
			var token = $$.getStorage("token")
			if (!token) {
				//去登录
				uni.reLaunch({
					url: '../login/login'
				})

			} else {
				defaultOption.header.Authorization = "Bearer " + token;
			}
		}
		return new Promise((reslove,reject)=>{
			uni.request({
				url:defaultOption.url,
				data:defaultOption.data,
				method:defaultOption.method,
				header:defaultOption.header,
				dataType: 'json',
				success: (res) => {
					
					if (res.statusCode == 200) {
						//判断登录超时或是统一操作判断
						if(res.data.code == 2000 || res.data.code == 2001){
							if (res.data.data && res.data.data.token) {
								$$.setStorage("token",res.data.data.token,0.1)
							}
							reslove(res.data) 
						}
						if(res.data.code == 4001){
							uni.reLaunch({
								url:"/pages/login/login"
							})
						}
						if(res.data.code == 500){
							uni.showToast({
								icon:"none",
								title: res.data.msg
							});
						}
					} else {
						uni.showToast({
							icon:"none",
							title: res.errMsg
						});
						reject(res)
					}
				},
				fail: (error) => {
					uni.showToast({
						icon: 'none',
						title: error.errMsg
					});
					reject(error)
				},
				complete: () => {
					uni.hideLoading()
				}
			})
		})
	},
	setStorage: function(key, val, expire) {
		let obj = {
			data: val,
			time: Date.now(),
			expire: expire * 60 * 60 * 1000 //天转换为毫秒
		};
		uni.setStorageSync(key, JSON.stringify(obj))
	},
	getStorage: function(key) {
		let val = uni.getStorageSync(key);
		if (!val) {
			return val;
		}
		val = JSON.parse(val);
		if (Date.now() - val.time > val.expire) {
			uni.removeStorageSync(key);
			return null;
		}
		return val.data;
	},
	isNotNull: function(obj) {
		if (
			!obj ||
			"null" === obj || {} === obj ||
			"{}" === obj ||
			"undefined" === obj ||
			"[object Object]" === obj || [] === obj ||
			"[]" === obj
		) {
			return false;
		}
		return true;
	},
	isEmptyObj: function(obj) {
		if ($$.isNotNull(obj)) {
			for (var key in obj) {
				return false;
			}
		}
		return true;
	}
}

import Vue from 'vue'
import App from './App'

import store from '@/store/index'
import {config,$$} from './common/js/config.js'

Vue.config.productionTip = false

Vue.prototype.config = config
Vue.prototype.$$ = $$

App.mpType = 'app'
// 视图层图片路径拼接
Vue.filter("joinImgPath",(url)=>{
	return url = config.IMGPATH + url
})
//时间转换
//rulestr为 链接符- 则只显示年月日，用链接符隔开
//rulestr为 冒号: 则显示完整时间，年月日用链接符隔开，时分秒用冒号
Vue.filter("timeFormat",(t,rulestr)=>{
	if(!t || !rulestr) return;
	rulestr = rulestr.toLowerCase()
	switch (rulestr){
		case 'yyyy-mm-dd':
			t = t.substring(0, 4) + '-' + t.substring(5, 7) + '-' + t.substring(8, 10)
			break
		case 'yyyy:mm:dd':
			t = t.substring(0, 4) + '/' + t.substring(5, 7) + '/' + t.substring(8, 10) + ' ' + t.substring(9, 11) + ':' + t.substring(12, 14) + ':' + t.substring(13, 15)
			break;
		case 'yyyy年mm月dd日':
			t = t.substring(0, 4) + '年' + t.substring(5, 7) + '月' + t.substring(8, 10) + '日'
			break
	}
	return t
})

const app = new Vue({
	store,
    ...App
})
app.$mount()

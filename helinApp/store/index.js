import Vue from 'vue'
import vuex from 'vuex'

Vue.use(vuex)

const store = new vuex.Store({
	state:{
		screenSize:{},
		user:'',
		logged:false,
		version:'1.0.0'
	},
	mutations:{
		setScreenSize(state,size){
			state.screenSize = size
		},
		setUser(state,value){
			state.user = value
		},
		setLogged(state,value){
			state.logged = value
		},
		setVersion(state,value){
			state.version = value
		}
	},
	actions:{
		getScreenSize({ commit },size){
			commit("setScreenSize",size)
		},
		getUser({ commit },user){
			commit("setUser",user)
		},
		getLogged({ commit },status){
			commit("setLogged",status)
		},
		getVersion({ commit },version){
			commit("setVersion",version)
		}
	}
})

export default store